<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\Util;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * Helper to get frequencies and transform them to XML
 * 
 * Source of frequencies : 
 * - http://browser.ihtsdotools.org/?perspective=full&conceptId1=307430002&edition=en-edition&release=v20170131&server=http://browser.ihtsdotools.org/api/snomed&langRefset=900000000000509007
 * - http://browser.ihtsdotools.org/?perspective=full&conceptId1=309611000&edition=en-edition&release=v20170131&server=http://browser.ihtsdotools.org/api/snomed&langRefset=900000000000509007
 * 
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AdministrationFrequencies
{
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    const DOMAIN = 'frequencies';
    
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * 
     * 
     * @return string[]
     */
    public function getFrequencies()
    {
        $values = array();
        
        // one to ten times a day
        for ($i=1; $i<=10; $i++) {
            $values[sprintf('%d-times-a-day', $i)] = $this->translator
                ->trans("%nb% times a day", [ '%nb%' => $i], self::DOMAIN);
        }
        
        // mealtimes
        foreach(['breakfast', 'evening', 'lunch', 'snack'] as $meal) {
            $values[sprintf('%s-mealtime', $meal)] = 
                $this->translator->trans(
                        sprintf("At %s time", $meal),
                        [],
                        self::DOMAIN
                    );
        }
        
        $values['end-of-the-day'] = $this->translator->trans("At the end of the "
            . "day", [], self::DOMAIN);
        $values['morning'] = $this->translator->trans("In the morning", [], 
            self::DOMAIN);
        
        return $values;
    }
    
    public function toEffectiveTime($code)
    {
        
    }
}
