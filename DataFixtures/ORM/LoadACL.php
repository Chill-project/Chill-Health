<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Chill\MainBundle\DataFixtures\ORM\LoadCenters;
//use Chill\MainBundle\DataFixtures\ORM\LoadPermissionsGroup;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Chill\MainBundle\Entity\User;
use Chill\HealthBundle\Security\Authorization\ConsultationVoter;
use Chill\MainBundle\Entity\RoleScope;
use Chill\MainBundle\Entity\Scope;
use Chill\MainBundle\Entity\GroupCenter;
use Chill\MainBundle\Entity\PermissionsGroup;
use Chill\HealthBundle\Security\Authorization\PublicationVoter;

/**
 * Load fixtures users into database
 * 
 * create a user for each permission_group and center.
 * username and password are identicals.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LoadACL extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     *
     * @var ContainerInterface
     */
    private $container;

    public function getOrder()
    {
        return 1001;
    }
    
    public static $scope = array(
            'names' => array(
                'fr' => 'médical',
                'en' => 'medical',
                'nl' => 'medisch'
            )
        );
    
    public static $permissionsGroup = array(
            'name' => 'medical',
            'role_scopes' => array(
                ConsultationVoter::UPDATE,
                ConsultationVoter::CREATE,
                PersonVoter::CREATE,
                PersonVoter::UPDATE,
                PublicationVoter::SEE,
                PublicationVoter::CREATE
            )
        );
    
    public static $groupCentersRefs = array();
    
    public static $medicalUsersRefs = array();

    public function load(ObjectManager $manager)
    {
        $this->loadCircle($manager);
        $this->loadPermissionsGroup($manager);
        $this->loadGroupCenters($manager);
        $this->loadUsers($manager);

        $manager->flush();
    }
    
    protected function loadCircle(ObjectManager $manager)
    {
        $circle = (new Scope())
            ->setName(static::$scope['names']);
        $manager->persist($circle);
        $this->setReference('chill_health_medical_circle', $circle);
    }
    
    protected function loadPermissionsGroup(ObjectManager $manager)
    {
        $permissionGroup = new PermissionsGroup();
        $permissionGroup->setName(static::$permissionsGroup['name']);
        
        foreach (static::$permissionsGroup['role_scopes'] as $role) {
            $roleScope = (new RoleScope())
                ->setRole($role)
                ->setScope($this->getReference('chill_health_medical_circle'))
                ;
            $manager->persist($roleScope);
            $permissionGroup->addRoleScope($roleScope);
        }

        $manager->persist($permissionGroup);
        $this->setReference('chill_health_permission_group', $permissionGroup);
    }
    
    protected function loadGroupCenters(ObjectManager $manager)
    {
        foreach (LoadCenters::$refs as $ref) {
            $center = $this->getReference($ref);
            $permissionGroup = $this->getReference('chill_health_'
                . 'permission_group');
            
            $groupCenter = (new GroupCenter())
                ->setCenter($center)
                ->setPermissionsGroup(
                    $permissionGroup
                    );
            
            $manager->persist($groupCenter);
            
            $groupCenterRef = 'chill_health_group_center_'.$ref.'_medical';
            $this->setReference($groupCenterRef, $groupCenter);
            static::$groupCentersRefs[] = $groupCenterRef;
        }
    }
    
    protected function loadUsers(ObjectManager $manager)
    {
        foreach (LoadCenters::$refs as $ref) {
            $center = $this->getReference($ref);
            $user = new User();
            $groupCenter = $this->getReference('chill_health_group_center_'.
                $ref.'_medical');

            $user->setUsername(\strtolower($center->getName()).'_medical')
                    ->setPassword(
                            $this->container->get('security.encoder_factory')
                                ->getEncoder($user)
                                ->encodePassword('password', $user->getSalt())
                            );

            $user->addGroupCenter($groupCenter);
            
            $manager->persist($user);
            
            echo 'Creating user ' . $user->getUsername()."... \n";
            $this->addReference($user->getUsername(), $user);
            static::$medicalUsersRefs[] = $user->getUsername();
        }
    }

    public function setContainer(ContainerInterface $container = null)
    {
        if (NULL === $container) {
            throw new \LogicException('$container should not be null');
        }
        
        $this->container = $container;
    }

}

