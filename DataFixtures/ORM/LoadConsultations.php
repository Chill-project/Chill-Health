<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Chill\HealthBundle\Entity\Consultation;
use Chill\HealthBundle\DataFixtures\ORM\LoadACL;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LoadConsultations extends AbstractFixture implements
    OrderedFixtureInterface, ContainerAwareInterface
{
    
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;
    
    public static $refs = array();
    
    public function getOrder()
    {
        return 56000;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr');
        $peoples = $this->container->get('chill.person.repository.person')
            ->findAll();
        
        for ($i = 0; $i < 150; $i++) {
            $author = $this->getReference(
                LoadACL::$medicalUsersRefs[array_rand(LoadACL::$medicalUsersRefs)]
                );
            $patient = $peoples[array_rand($peoples)];
            $states = [Consultation::STATE_CLOSED, Consultation::STATE_DRAFT];
            
            $consultation = (new Consultation())
                ->setAuthor($author)
                ->setCircle($this->getReference('chill_health_medical_circle'))
                ->setDate($faker->dateTimeBetween('-5 years', 'now'))
                ->setPatient($patient)
                ->setState($states[\array_rand($states)])
                ;
            
            $manager->persist($consultation);
            $ref = 'chill_health_'.$consultation->getId();
            $this->setReference($ref, $consultation);
            static::$refs[] = $ref;
        }
        
        $manager->flush();
    }
}

