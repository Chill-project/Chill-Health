/* 
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* jslint vars: true */
/* jslint indent: 4 */
/* global document, window  */
'use strict';

var MedicationForm = function() {
    
    // default locale is fr
    // currently unused due to binding problem
    var locale = "fr";
    
    function displayCodeData(select) {
        
        var  
            description_div = document.getElementById("substance_description_container"),
            code, 
            substance_selected,
            ul_ampps;
    
        if (select.value !== 'undefined') {
            substance_selected = select.querySelector(['[value="' + select.value + '"]']);
        } else {
            return;
        }
    
        if (substance_selected !== null) {
            code = JSON.parse(substance_selected.dataset.code);
        } else {
            return;
        }
        
        // reset content
        description_div.innerHTML = "";
        
        if (code.hasOwnProperty("ampps") && code.ampps.length > 0) {
            description_div.innerHTML += "<h4>Packages</h4>";
            ul_ampps = document.createElement('ul');
            description_div.appendChild(ul_ampps);
            
            for (let i=0; i < code.ampps.length; i = i+1) {
                let li = document.createElement('li');
                ul_ampps.appendChild(li);
                
                if (code.ampps[i].hasOwnProperty('prescriptionName')) {
                    let span = document.createElement('span');
                    span.classList.add('chill_health_medication_prescription_name');
                    li.appendChild(span);
                    li.appendChild(document.createTextNode(" "));
                    span.innerHTML = this._getLocalizedString(code.ampps[i].prescriptionName);
                } else {
                    let span = document.createElement('span');
                    span.classList.add('chill-no-data-statement');
                    span.innerHTML = "Nom de prescription inconnu ";
                    li.appendChild(span);
                }
                
                if (code.ampps[i].hasOwnProperty('crmLink')) {
                    let a = document.createElement('a');
                    a.href = this._getLocalizedString(code.ampps[i].crmLink);
                    a.innerHTML = '<i class="fa fa-eur" aria-hidden="true"></i>';
                    a.target = '_blank';
                    li.appendChild(a);
                    li.appendChild(document.createTextNode(" "));
                }
                if (code.ampps[i].hasOwnProperty('leaflets')) {
                    let a = document.createElement('a');
                    a.href = this._getLocalizedString(code.ampps[i].leaflets);
                    a.innerHTML = '<i class="fa fa-file-text" aria-hidden="true"></i>';
                    a.target = '_blank';
                    li.appendChild(a);
                    li.appendChild(document.createTextNode(" "));
                }
            }
            
        } 

        if (code.hasOwnProperty('ampComponents')) { 
            for (let iComponents = 0; iComponents < code.ampComponents.length; iComponents = iComponents + 1) { 
                let component = code.ampComponents[iComponents];
                
                if (component.hasOwnProperty("realActualIngredients")) {
                    let title = document.createElement('h4');
                    title.appendChild(document.createTextNode('Composants'));
                    description_div.appendChild(title);

                    let ul_ingredients = document.createElement('ul');
                    description_div.appendChild(ul_ingredients);

                    for (let i = 0; i < component.realActualIngredients.length; i = i+1) {
                        let li = document.createElement('li');
                        let span = document.createElement('span');
                        span.classList.add('chill_health_substance_name');
                        span.innerHTML = this._getLocalizedString(component.realActualIngredients[i].substance.label);
                        li.appendChild(span);
                        ul_ingredients.appendChild(li);
                    }
                }
        }
        } 
        

    }
    
    function _getLocalizedString(object) {
        
        var l;

        if (object.hasOwnProperty("fr")) {
            return object["fr"];
        }
        
        // return the first element in loop if "fr" is not found
        for (l in object) {
            return object[l];
        }
        
        return null;
    }
    
    return {
        displayCodeData: displayCodeData,
        _getLocalizedString: _getLocalizedString
    };
}();
