<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\Controller;

use Chill\HealthBundle\Entity\Consultation;
use Chill\HealthBundle\Security\Authorization\ConsultationVoter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Chill\HealthBundle\Entity\Medication;
use Chill\HealthBundle\Form\MedicationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class MedicationController extends Controller
{
    public function createAction(Request $request)
    {
        $form = $this->createMedicationCreateForm();
        
        $form->handleRequest($request);
        
        /* @var $medication Medication */
        $medication = $form->getData();

        if ($form->isValid()) {
            // we can have access to consultation, so we check the access
            // only now
            $this->denyAccessUnlessGranted(ConsultationVoter::UPDATE, 
                $medication->getConsultation(), "You are not allowed to update"
                . " this consultation");
            
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($medication);
            $em->flush();
            
            $this->addFlash('success', $this->get('translator')->trans("A "
                . "medication is successfully created"));
            
            return $this->redirectToRoute('chill_health_consultation_show', [
                    'consultation_id' => $medication->getConsultation()
                            ->getId(),
                    'action' => 'medication_highlight',
                    'medication_id' => $medication->getId(),
                    '_fragment' => 'medication-'.$medication->getId()
                ]);
            
        }
        
        $this->denyAccessUnlessGranted(ConsultationVoter::SEE, 
            $medication->getConsultation(), "You are not allowed to see this "
            . "consultation");
        
        return $this->render("ChillHealthBundle:Consultation:show.html.twig",
            array(
                    'person' => $medication->getConsultation()->getPatient(),
                    'consultation' => $medication->getConsultation(),
                    'form_medication' => $form->createView(),
                ));
    }
    
    public function updateAction($medication_id, Request $request)
    {
        /* @var $medication Medication */
        $medication = $this->get('chill_health.repository_medication')
            ->find($medication_id);
        
        
        if ($medication === null) {
            throw $this->createNotFoundException("medication not found");
        }
        
        $this->denyAccessUnlessGranted(ConsultationVoter::UPDATE, 
            $medication->getConsultation());
        
        $form = $this->createMedicationUpdateForm($medication);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            // we can have access to consultation, so we check the access
            // only now
            $this->denyAccessUnlessGranted(ConsultationVoter::UPDATE, 
                $medication->getConsultation(), "You are not allowed to update"
                . " this consultation");
            
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', $this->get('translator')->trans("A "
                . "medication is successfully updated"));
            
            return $this->redirectToRoute('chill_health_consultation_show', [
                    'consultation_id' => $medication->getConsultation()
                            ->getId(),
                    'action' => 'medication_highlight',
                    'medication_id' => $medication->getId(),
                    '_fragment' => 'medication-'.$medication->getId()
                ]);
            
        }
        
        $this->denyAccessUnlessGranted(ConsultationVoter::SEE, 
            $medication->getConsultation(), "You are not allowed to see this "
            . "consultation");
        
        return $this->render("ChillHealthBundle:Consultation:show.html.twig",
            array(
                    'person' => $medication->getConsultation()->getPatient(),
                    'consultation' => $medication->getConsultation(),
                    'form_medication' => $form->createView(),
                ));
    }
    
        /**
     * 
     * @param \Chill\HealthBundle\Controller\Consultation $consultation
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createMedicationCreateForm()
    {
        return $this->createForm(
                MedicationType::class, 
                new Medication()
                ,
                [
                    'action' => $this
                        ->generateUrl('chill_health_medication_create'),
                    'method' => 'POST'
                ]
            )
            ->add('submit', SubmitType::class);
    }
    
    /**
     * 
     * @param Medication $medication
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createMedicationUpdateForm(Medication $medication)
    {
        return $this->createForm(
                MedicationType::class, 
                $medication
                ,
                [
                    'action' => $this
                        ->generateUrl('chill_health_medication_update', [
                            'medication_id' => $medication->getId()
                        ]),
                    'method' => 'POST'
                ]
            )
            ->add('submit', SubmitType::class)
            ;
    }
}
