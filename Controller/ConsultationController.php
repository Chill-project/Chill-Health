<?php

namespace Chill\HealthBundle\Controller;

use Chill\HealthBundle\Form\CloseConsultationType;
use Chill\HealthBundle\Entity\Consultation;
use Chill\HealthBundle\Security\Authorization\ConsultationVoter;
use Chill\HealthBundle\Form\ConsultationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Chill\HealthBundle\Entity\Medication;
use Chill\HealthBundle\Form\MedicationType;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ConsultationController extends Controller
{
    /**
     * 
     * @param int $id personId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws type
     */
    public function listAction($person_id)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        /* @var $authorizationHelper \Chill\MainBundle\Security\Authorization\AuthorizationHelper */
        $authorizationHelper = $this->get('chill.main.security.'
            . 'authorization.helper');
        
        $circles = $authorizationHelper->getReachableCircles(
            $this->getUser(), 
            new Role(ConsultationVoter::SEE), 
            $person->getCenter()
            );
        
        $consultations = $this->getDoctrine()->getManager()
            ->createQuery('SELECT c FROM ChillHealthBundle:Consultation c '
                . 'WHERE c.patient = :person AND c.circle IN(:circles) '
                . 'ORDER BY c.date DESC')
            ->setParameter('person', $person)
            ->setParameter('circles', $circles)
            ->getResult();
        
        return $this->render('ChillHealthBundle:Consultation:list.html.twig', array(
                'person' => $person,
                'consultations' => $consultations
            ));    
    }

    public function newAction($person_id)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        $form = $this->createCreateForm(
            (new Consultation())
            ->setPatient($person)
            ->setDate(new \DateTime())
            );
        
        return $this->render('ChillHealthBundle:Consultation:new.html.twig', array(
                'person' => $person,
                'form'   => $form->createView()
            ));    
        
    }
    
    public function createAction($person_id, Request $request)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        $consultation = (new Consultation())
            ->setPatient($person)
            ->setDate(new \DateTime())
            ;
        
        $form = $this->createCreateForm($consultation);
        
        $this->denyAccessUnlessGranted(ConsultationVoter::CREATE, $consultation);
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($consultation);
            $em->flush();
            
            $this->addFlash('success', $this->get('translator')
                ->trans("The consultation is created."));
            
            return $this->redirectToRoute('chill_health_consultation_show', [
                'consultation_id' => $consultation->getId()
            ]);
        }
        
        return $this->render('ChillHealthBundle:Consultation:new.html.twig', array(
                'person' => $person,
                'form'   => $form->createView()
            )); 
    }
    
    public function editAction($consultation_id, Request $request)
    {
        /* @var $consultation Consultation */
        $consultation = $this->get('chill_health.repository_consultation')
            ->find($consultation_id);
        
        if ($consultation === null) {
            throw $this->createNotFoundException("consultation not found");
        }
        
        $this->denyAccessUnlessGranted(ConsultationVoter::UPDATE, $consultation);
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $consultation->getPatient());
        
        $form = $this->createUpdateForm($consultation);
        
        return $this->render('ChillHealthBundle:Consultation:edit.html.twig', array(
                'consultation' => $consultation,
                'form'   => $form->createView()
            )); 
    }
    
    public function updateAction($consultation_id, Request $request)
    {
        /* @var $consultation Consultation */
        $consultation = $this->get('chill_health.repository_consultation')
            ->find($consultation_id);
        
        if ($consultation === null) {
            throw $this->createNotFoundException("consultation not found");
        }
        
        $this->denyAccessUnlessGranted(ConsultationVoter::UPDATE, $consultation);
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $consultation->getPatient());
        
        $form = $this->createUpdateForm($consultation);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', $this->get('translator')
                ->trans("The consultation is updated."));
            
            return $this->redirectToRoute('chill_health_consultation_show', [
                'consultation_id' => $consultation->getId()
            ]);
        }
        
        return $this->render('ChillHealthBundle:Consultation:edit.html.twig', array(
                'consultation' => $consultation,
                'form'   => $form->createView()
            )); 
    }
    
    public function showAction($consultation_id, Request $request)
    {   
        /* @var $consultation \Chill\HealthBundle\Entity\Consultation */
        $consultation = $this->get('chill_health.repository_consultation')
            ->find($consultation_id);
        
        if ($consultation === null) {
            throw $this->createNotFoundException("consultation not found");
        }
        
        $this->denyAccessUnlessGranted(ConsultationVoter::SEE, $consultation);
        
        $person = $consultation->getPatient();
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        // embed the form if required
        $params = array();
        if ($request->query->has('action')) {
            switch ($request->query->get('action')) {
                case 'medication_add':
                    $params['form_medication'] = $this
                        ->createMedicationCreateForm($consultation)
                        ->createView();
                    $params['form_medication_step'] = 'create';
                    break;
                case 'medication_update':
                    $medication = $consultation->getMedications()
                        ->filter(function(Medication $m) use ($request) { 
                            return $request->query->get('medication_id') === $m->getId();
                        })
                        ->first();
                    // remove from consultation temporarily, to not to show here
                    $consultation->removeMedication($medication);
                    $params['form_medication'] = $this
                        ->createMedicationUpdateForm($medication)
                        ->createView();
                    $params['form_medication_step'] = 'update';
                    break;
                case 'medication_highlight':
                    $params['medication_highlight'][] = $request->query
                        ->getInt('medication_id', null);
                    break;
                case 'suggest_close':
                    $params['form_close'] = $this
                        ->createConsultationCloseForm($consultation, false)
                        ->createView();
                    break;
                case 'suggest_close_and_publish':
                    $params['form_close'] = $this
                        ->createConsultationCloseForm($consultation, true)
                        ->createView();
                default:
                    throw new \RuntimeException(sprintf("The action '%s' "
                        . "is unknown", $request->query->get('action')));
            }
        }
        
        return $this->render("ChillHealthBundle:Consultation:show.html.twig",
            \array_merge(
                array(
                    'person' => $person,
                    'consultation' => $consultation
                ), 
                $params)
            );
    }
    
    /**
     * Action to close the consultation
     * 
     * 
     * @param int $consultation_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception 
     */
    public function closeAction($consultation_id, Request $request)
    {
        /* @var $consultation \Chill\HealthBundle\Entity\Consultation */
        $consultation = $this->get('chill_health.repository_consultation')
            ->find($consultation_id);
        
        if ($consultation === null) {
            throw $this->createNotFoundException("consultation not found");
        }
        
        $this->denyAccessUnlessGranted(ConsultationVoter::SEE, $consultation);
        
        $person = $consultation->getPatient();
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        $form = $this->createConsultationCloseForm($consultation, 
            $request->query->getInt('publish', 1) === 1);
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $consultation->setState(Consultation::STATE_CLOSED);
            
            $this->getDoctrine()->getManager()
                ->flush();
            
            $this->addFlash(('success'), $this->get('translator')
                ->trans('The consultation is closed.'));
            
            if ($request->query->getInt('publish', 0) === 0) {
                return $this->redirectToRoute('chill_health_consultation_show', 
                    [
                        'consultation_id' => $consultation->getId()
                    ]);
            } else {
                return $this->redirectToRoute('chill_brusafe_document_new',
                    [
                        'consultation_id' => $consultation->getId()
                    ]);
            }
        }
        
        $this->addFlash('error', $this->get('translator')
            ->trans('The consultation could not be closed.'));
        
        return $this->redirectToRoute('chill_health_consultation_show', 
            [
                'consultation_id' => $consultation->getId(),
                'action' => 'suggest_close'
            ]);
    }
    
    /**
     * Create a create form
     * 
     * @param Consultation $consultation
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createCreateForm(Consultation $consultation)
    {
        return $this->createForm(
            ConsultationType::class,
            $consultation,
            [
                'center' => $consultation->getPatient()->getCenter(),
                'step' => ConsultationType::CREATE,
                'action' => $this->generateUrl(
                    'chill_health_consultation_create',
                    [ 'person_id' => $consultation->getPatient()->getId() ]
                    )
            ]
            )
            ->add('submit', SubmitType::class)
            ;
    }
    
    /**
     * Create an update form
     * 
     * @param Consultation $consultation
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createUpdateForm(Consultation $consultation)
    {
        return $this->createForm(
            ConsultationType::class,
            $consultation,
            [
                'center' => $consultation->getPatient()->getCenter(),
                'step' => ConsultationType::UPDATE,
                'action' => $this->generateUrl(
                    'chill_health_consultation_update',
                    [ 'consultation_id' => $consultation->getId() ]
                    )
            ]
            )
            ->add('submit', SubmitType::class)
            ;
    }
    
    /**
     * 
     * @param \Chill\HealthBundle\Controller\Consultation $consultation
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createMedicationCreateForm(Consultation $consultation)
    {
        return $this->createForm(
                MedicationType::class, 
                (new Medication())
                    ->setConsultation($consultation)
                    ->setDateFrom(new \DateTime())
                    ->setDateTo((new \DateTime())->add(new \DateInterval('P3D')))
                ,
                [
                    'action' => $this
                        ->generateUrl('chill_health_medication_create'),
                    'method' => 'POST'
                ]
            )
            ->add('submit', SubmitType::class)
            ;
    }
    
    /**
     * 
     * @param Medication $medication
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createMedicationUpdateForm(Medication $medication)
    {
        return $this->createForm(
                MedicationType::class, 
                $medication
                ,
                [
                    'action' => $this
                        ->generateUrl('chill_health_medication_update', [
                            'medication_id' => $medication->getId()
                        ]),
                    'method' => 'POST'
                ]
            )
            ->add('submit', SubmitType::class)
            ;
    }
    
    /**
     * Create a form to close consultation
     * 
     * @param Consultation $consultation
     * @param boolean $goToPublish Wether the next step should be a publish action
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createConsultationCloseForm(
        Consultation $consultation,
        $goToPublish
    ) {
        $builder = $this->createFormBuilder(
            [
                'consultation' => $consultation->getId(),
                'submit' => 'close'
            ], 
            [
                'action' => $this->generateUrl(
                    'chill_health_consultation_close',
                    [
                        'consultation_id' => $consultation->getId(),
                        'publish' => $goToPublish ? '1' : '0'
                    ]),
                'method' => 'POST'
        ])
            ->add('consultation', HiddenType::class)
            ->add('submit', SubmitType::class);
        
        return $builder->getForm();
    }

}
