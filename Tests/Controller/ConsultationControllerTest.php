<?php


namespace Chill\HealthBundle\Tests\Controller;

require_once('/home/julien/dev/chill-dev/vendor/chill-project/main/Test/PrepareClientTrait.php');


use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ConsultationControllerTest extends WebTestCase
{
    use \Chill\MainBundle\Test\PrepareClientTrait;
    
    /**
     *
     * @var ObjectManager
     */
    protected $em;
    
    public function setUp()
    {
        static::bootKernel();
        
        $this->em = static::$kernel->getContainer()
            ->get('doctrine.orm.entity_manager');
    }
    
    public function testList()
    {
        $person = $this->em->createQuery(""
            . "SELECT p "
            . "FROM ChillPersonBundle:Person p "
            . "JOIN p.center center "
            . "WHERE p.id IN ("
            . "SELECT DISTINCT IDENTITY(c.patient) "
            . "FROM ChillHealthBundle:Consultation c "
            . ")"
            . "AND center.name = :center_name "
            )
            ->setParameter('center_name', 'Center A')
            ->setMaxResults(1)
            ->getSingleResult();
        
        $client = $this->getClient('center a_medical');

        $crawler = $client->request('GET', 
            sprintf('/fr/person/%s/health/consultations', $person->getId()));
        
         $this->assertGreaterThan(0, 
             $crawler->filter('table tbody tr')->count());
    }

    public function testNew()
    {
        $this->markTestIncomplete();
        //$client = static::createClient();

        //$crawler = $client->request('GET', '/person/{id}/health/consultation/new');
    }

}
