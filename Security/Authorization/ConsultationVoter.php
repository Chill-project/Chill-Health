<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\HealthBundle\Security\Authorization;

use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\HealthBundle\Entity\Consultation;

/**
 * Check the Access Model for consultation.
 * 
 * This class also check the consultation state :
 * 
 * - if the consultation is closed => the consultation is not allowed to be 
 *   updated ;
 * - if the consultation is draft => we check that the user has the required
 *   access
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ConsultationVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    const CREATE = 'CHILL_HEALTH_CONSULTATION_CREATE';
    const SEE    = 'CHILL_HEALTH_CONSULTATION_SEE';
    const UPDATE = 'CHILL_HEALTH_CONSULTATION_UPDATE';
    
    /**
     *
     * @var AuthorizationHelper
     */
    protected $helper;
    
    public function __construct(AuthorizationHelper $helper)
    {
        $this->helper = $helper;
    }
    
    protected function getSupportedAttributes()
    {
        return [self::CREATE, self::SEE, self::UPDATE];
    }

    protected function getSupportedClasses()
    {
        return [Consultation::class];
    }

    protected function isGranted($attribute, $consultation, $user = null)
    {
        if (! $user instanceof \Chill\MainBundle\Entity\User) {
            return false;
        }
        
        if ($attribute === self::UPDATE) {
            if ($consultation->getState() === Consultation::STATE_CLOSED) {
                return false;
            }
        }
        
        return $this->helper->userHasAccess($user, $consultation, $attribute);
    }

    public function getRoles()
    {
        return $this->getSupportedAttributes();
    }

    public function getRolesWithoutScope()
    {
        return array();
    }
    
    public function getRolesWithHierarchy()
    {
        return [ 'Medical consultations' => $this->getRoles() ];
    }
}
