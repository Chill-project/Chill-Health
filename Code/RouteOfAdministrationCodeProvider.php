<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\Code;

use Chill\HealthBundle\Entity\Code;

/**
 * Provide code for Route Of Administration
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class RouteOfAdministrationCodeProvider
{
    /**
     * @return \Iterator
     */
    public function getCodes()
    {
        $doc = new \DOMDocument("1.0");
        
        $doc->loadXML(self::$xml);
        $list = $doc->getElementsByTagNameNS("urn:ihe:iti:svs:2008", 'Concept');
        
        foreach ($list as $el) {
            yield (new Code())
                ->setCode($el->attributes
                    ->getNamedItem("code")->nodeValue)
                ->setCodeSystem($el->attributes
                    ->getNamedItem("codeSystem")->nodeValue)
                ->setDisplayName(["en" => 
                    $el->attributes->getNamedItem("displayName")->nodeValue])
                ->setType('route_of_administration')
                ;
        }
    }
    
    /**
     * xml list of concepts for route of administration
     * 
     * Retrieved from https://gazelle.ehealth.brussels/SVSSimulator/rest/RetrieveValueSetForSimulator?id=2.16.840.1.114222.4.11.816
     *
     * @var string
     */
    private static $xml = <<<XML
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<RetrieveValueSetResponse xmlns="urn:ihe:iti:svs:2008">
    <ValueSet displayName="Route of Administration (HL7)" version="20111130" id="2.16.840.1.114222.4.11.816">
        <ConceptList xml:lang="en-US">
            <Concept code="AP" codeSystem="2.16.840.1.113883.12.162" displayName="Apply Externally"/>
            <Concept code="B" codeSystem="2.16.840.1.113883.12.162" displayName="Buccal"/>
            <Concept code="DT" codeSystem="2.16.840.1.113883.12.162" displayName="Dental"/>
            <Concept code="EP" codeSystem="2.16.840.1.113883.12.162" displayName="Epidural"/>
            <Concept code="ET" codeSystem="2.16.840.1.113883.12.162" displayName="Endotrachial Tube"/>
            <Concept code="GTT" codeSystem="2.16.840.1.113883.12.162" displayName="Gastrostomy Tube"/>
            <Concept code="GU" codeSystem="2.16.840.1.113883.12.162" displayName="GU Irrigant"/>
            <Concept code="IA" codeSystem="2.16.840.1.113883.12.162" displayName="Intra-arterial"/>
            <Concept code="IB" codeSystem="2.16.840.1.113883.12.162" displayName="Intrabursal"/>
            <Concept code="IC" codeSystem="2.16.840.1.113883.12.162" displayName="Intracardiac"/>
            <Concept code="ICV" codeSystem="2.16.840.1.113883.12.162" displayName="Intracervical (uterus)"/>
            <Concept code="ID" codeSystem="2.16.840.1.113883.12.162" displayName="Intradermal"/>
            <Concept code="IH" codeSystem="2.16.840.1.113883.12.162" displayName="Inhalation"/>
            <Concept code="IHA" codeSystem="2.16.840.1.113883.12.162" displayName="Intrahepatic Artery"/>
            <Concept code="IM" codeSystem="2.16.840.1.113883.12.162" displayName="Intramuscular"/>
            <Concept code="IMR" codeSystem="2.16.840.1.113883.12.162" displayName="Immerse (Soak) Body Part"/>
            <Concept code="IN" codeSystem="2.16.840.1.113883.12.162" displayName="Intranasal"/>
            <Concept code="IO" codeSystem="2.16.840.1.113883.12.162" displayName="Intraocular"/>
            <Concept code="IP" codeSystem="2.16.840.1.113883.12.162" displayName="Intraperitoneal"/>
            <Concept code="IS" codeSystem="2.16.840.1.113883.12.162" displayName="Intrasynovial"/>
            <Concept code="IT" codeSystem="2.16.840.1.113883.12.162" displayName="Intrathecal"/>
            <Concept code="IU" codeSystem="2.16.840.1.113883.12.162" displayName="Intrauterine"/>
            <Concept code="IV" codeSystem="2.16.840.1.113883.12.162" displayName="Intravenous"/>
            <Concept code="MM" codeSystem="2.16.840.1.113883.12.162" displayName="Mucous Membrane"/>
            <Concept code="MTH" codeSystem="2.16.840.1.113883.12.162" displayName="Mouth/Throat"/>
            <Concept code="NG" codeSystem="2.16.840.1.113883.12.162" displayName="Nasogastric"/>
            <Concept code="NP" codeSystem="2.16.840.1.113883.12.162" displayName="Nasal Prongs"/>
            <Concept code="NS" codeSystem="2.16.840.1.113883.12.162" displayName="Nasal"/>
            <Concept code="NT" codeSystem="2.16.840.1.113883.12.162" displayName="Nasotrachial Tube"/>
            <Concept code="OP" codeSystem="2.16.840.1.113883.12.162" displayName="Ophthalmic"/>
            <Concept code="OT" codeSystem="2.16.840.1.113883.12.162" displayName="Otic"/>
            <Concept code="OTH" codeSystem="2.16.840.1.113883.12.162" displayName="Other/Miscellaneous"/>
            <Concept code="PF" codeSystem="2.16.840.1.113883.12.162" displayName="Perfusion"/>
            <Concept code="PO" codeSystem="2.16.840.1.113883.12.162" displayName="Oral"/>
            <Concept code="PR" codeSystem="2.16.840.1.113883.12.162" displayName="Rectal"/>
            <Concept code="RM" codeSystem="2.16.840.1.113883.12.162" displayName="Rebreather Mask"/>
            <Concept code="SC" codeSystem="2.16.840.1.113883.12.162" displayName="Subcutaneous"/>
            <Concept code="SD" codeSystem="2.16.840.1.113883.12.162" displayName="Soaked Dressing"/>
            <Concept code="SL" codeSystem="2.16.840.1.113883.12.162" displayName="Sublingual"/>
            <Concept code="TD" codeSystem="2.16.840.1.113883.12.162" displayName="Transdermal"/>
            <Concept code="TL" codeSystem="2.16.840.1.113883.12.162" displayName="Translingual"/>
            <Concept code="TP" codeSystem="2.16.840.1.113883.12.162" displayName="Topical"/>
            <Concept code="TRA" codeSystem="2.16.840.1.113883.12.162" displayName="Tracheostomy"/>
            <Concept code="UR" codeSystem="2.16.840.1.113883.12.162" displayName="Urethral"/>
            <Concept code="VG" codeSystem="2.16.840.1.113883.12.162" displayName="Vaginal"/>
            <Concept code="VM" codeSystem="2.16.840.1.113883.12.162" displayName="Ventimask"/>
            <Concept code="WND" codeSystem="2.16.840.1.113883.12.162" displayName="Wound"/>
        </ConceptList>
    </ValueSet>
</RetrieveValueSetResponse>
XML;
}
