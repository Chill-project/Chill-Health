<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\Code;

use Chill\HealthBundle\Entity\Code;
use Psr\Log\LoggerInterface;

/**
 * This class import the SAM code for the drugs provided by sam-civics. 
 * 
 * Only drug with at least one product package available in commerce is kept.
 * 
 * The imported elements are :
 * 
 * - label: the name of the product, in 4 languages (en, fr, nl, de) + the substance name;
 * - code: the SAM code
 * 
 * and more data are stored :
 * 
 * ```
 * {
 *  // components of the drug
 *  "ampComponents": [ {
 *      // we keep only data for today.
 *      "pharmaceuticalForms": [{ "code": "3305", "label": { "fr": "///" } }],
 *      "routesOfAdministration": [
 *            { "code": "71", "label": { "fr": "///" }
 *        ],
 *      "realActualIngredients": [
 *         { 
 *             "type": "ACTIVE_SUBSTANCE", 
 *              "substance": { "code": "456", "label": { "fr": "///" } }
 *         }
 *      ]
 *   }],
 *   // packages available in pharmacists
 *   "ampps": [{
 *      "leaflets": { "fr": "http://etc." } ,
 *      "prescriptionName": { "fr": "///" },
 *      "crmLink": { "fr": "http", "nl": "http" },
 *      "dmpps": [{
 *          "cnk": "12345678"
 *      }],
 *   }]
 * }
 * ```
 * 
 * The data of the element are all valid at the time of processing.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CNKDrugCodeProvider
{
    
    const FILE_NAME = 'AMP.xml';
    const CODE_SYSTEM = 'CNK';
    
    const NS_SAMWS_COMMON = "urn:be:fgov:ehealth:samws:v2:actual:common";
    const NS_SAMWS_EXPORT = "urn:be:fgov:ehealth:samws:v2:export";
    const NS_SAMWS_CORE   = "urn:be:fgov:ehealth:samws:v2:core";
    const NS_SAMWS_REFDATA = "urn:be:fgov:ehealth:samws:v2:refdata";
    
    /**
     * the date format used in the xml
     */
    const DATE_FORMAT = 'Y-m-d';
    
    /**
     * the file path of the source. Builded by constructor
     *
     * @var string
     */
    protected $source_file_path;
    
    /**
     * the today's date
     * 
     * @var \DateTime
     */
    protected $now ;
    
    /**
     *
     * @var LoggerInterface
     */
    protected $logger;
    
    public function __construct(LoggerInterface $logger)
    {
        $this->now = new \DateTime('now');
        $this->logger = $logger;
        $this->source_file_path = __DIR__.'/'.self::FILE_NAME;
    }
    
    
    
    /**
     * @return \Iterator
     */
    public function getCodes()
    {
        $this->logger->debug('starting import of CNK drug codes');
        /* @var $codes int[] will store codes handled during this script */
        $codes = [];
        
        $dom = new \DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->load($this->source_file_path);
        
        $this->logger->debug('The data is loaded.');
        
        $i = 0;
        foreach ($dom->getElementsByTagNameNS(self::NS_SAMWS_EXPORT, 'Amp') as $el) {
            $this->logger->debug('processing Amp #'.$i.' with SAM '
                .$el->getAttribute('code'));
            
            $code = $this->processElement($el);
            
            if ($code === null) {
                $this->logger->debug("AMP #$i is null, skipping");
            }
            
            $i++;
            
            if ($code !== null) {
                yield $code;
            } else {
                continue;
            }
        }
        
        $this->logger->info("$i amp elements handled");
        
    }
    
    /**
     * Process a single AMP element in XML, and return it as a code
     * 
     * @param \DOMElement $el
     * @return Code
     */
    protected function processElement(\DOMElement $el)
    {
        $ampData = $this->findActualNode($el, self::NS_SAMWS_EXPORT, 'Data');
        $ampps = $this->findAmppWithDmpp($el);
        $ampComponents = $this->findAmpComponents($el);
        $samCode = $el->getAttribute("code");
        
        // check that we have at least one amp
        if ($ampData === null) {
            $this->logger->debug('no ampData for '.$samCode);
            return null;
        }
        
        // check the status, stop if not authorized
        $statusList = $ampData->getElementsByTagNameNS(self::NS_SAMWS_COMMON, 
            'Status');
        if ($statusList->length === 0) {
            $this->logger->notice(sprintf("Drug with SAM $samCode hasn't any status,"
                . " skipping"));
            die();
            return null;
        }
        
        if ($ampData->getElementsByTagNameNS(self::NS_SAMWS_COMMON, 'Status')
            ->item(0)->nodeValue !== 'AUTHORIZED') {
            $this->logger->notice(sprintf("Drug with SAM $samCode is not AUTHORIZED,"
                . " skipping"));
            return null;
        }
        
        // check that the element is commercialized (has at least one CNK code
        if (count($ampps) === 0) {
            $this->logger->notice(sprintf("Drug with SAM $samCode hasn't any ampp,"
                . " skipping"));
            
            return null;
        }
        
        $code = (new Code())
            ->setDisplayName($this->buildDisplayName($ampData))
            ->setCodeSystem('2.25.166530137506592448033065606534052787292')
            ->setCode($samCode)
            ->setData($this->buildData($el, $ampComponents, $ampps))
            ->setType('substance')
            ;
        
        $this->logger->debug(sprintf("Creating drug with SAM: %s, "
            . "label: %s, data: %s", $code->getCode(), 
            \json_encode($code->getDisplayName()),
            \json_encode($code->getData())));
        
        return $code;
    }
    
    /**
     * 
     * @param \DOMElement $amp
     * @param \DOMElement[] $ampComponents
     * @param \DOMElement[] $ampps
     */
    protected function buildData(
        \DOMElement $amp, 
        array $ampComponents, 
        array $ampps
    ) {
        $data = array();
        
        foreach ($ampComponents as $ampComponent) {
            // get the Data element valid for today
            $ampComponentData = $this->findActualNode($ampComponent, 
                self::NS_SAMWS_EXPORT, 
                'Data');
            // create a new entry in data
            $dataComponent = array();
            
            foreach ($ampComponentData->childNodes as $child) {
                /* @var $child \DOMElement */
            
                // get the pharmaceutical forms
                if ($child->localName === 'PharmaceuticalForma' and 
                    $child->namespaceURI === self::NS_SAMWS_EXPORT) {
                        /* @var $pharmaceuticalForm \DOMElement */

                    $pharmaceuticalForm === $child;

                    $dataComponent['pharmaceuticalForms'][] = [
                        'code' => $pharmaceuticalForm->getAttribute('code'),
                        'label' => $this->buildTranslatableString(
                            $pharmaceuticalForm->firstChild // should be 'Name'
                            )
                        ];
                }

                // get the route of administration
                if ($child->namespaceURI === self::NS_SAMWS_EXPORT and
                    $child->localName === 'RouteOfAdministration') {

                    $dataComponent['routeOfAdministrations'][] = [
                        'code' => $child->getAttribute('code'),
                        'label' => $this->buildTranslatableString(
                            $child->firstChild // should be 'Name'
                            )
                    ];
                }
            }
            
            foreach ($ampComponent->childNodes as $child) {
                // get the realActualIngredients
                if ($child->namespaceURI === self::NS_SAMWS_EXPORT and  
                    $child->localName === 'RealActualIngredient') {

                    $realActualIngredientData = $this->findActualNode(
                        $child, self::NS_SAMWS_EXPORT, 'Data');
                    /* @var $substance \DOMElement */
                    if ($realActualIngredientData !== null) {
                        
                        // reset strength, not required in schema
                        $strength = null;
                        
                        foreach ($realActualIngredientData->childNodes as 
                            $realActualIngredientDataChildNode) {
                            if ($realActualIngredientDataChildNode->namespaceURI === self::NS_SAMWS_EXPORT
                                and $realActualIngredientDataChildNode->localName === 'Substance') {
                                $substance = $realActualIngredientDataChildNode;
                            }
                            
                            if ($realActualIngredientDataChildNode->localName === 'Strength') {
                                $strength = $realActualIngredientDataChildNode;
                            }
                            
                            if ($realActualIngredientDataChildNode->localName === 'Type') {
                                $type = $realActualIngredientDataChildNode;
                            }
                        }

                         $d = [
                            "type" => $type->nodeValue,
                            "substance" => [
                                "label" => $this->buildTranslatableString(
                                    $substance->firstChild
                                    ),
                                "code"  => $substance->getAttribute('code')
                            ]
                        ];
                        
                        if ($strength !== null) {
                            $d['strength'] = [
                                "unit" => $strength->getAttribute('unit'),
                                "value" => (float) $strength->nodeValue
                            ];
                        }
                        
                        $dataComponent['realActualIngredients'][] = $d;
                    }
                } 
            
            }
            
            $data['ampComponents'][] = $dataComponent;
        }
        
        // package available in pharmacist (ampp)
        foreach ($ampps as $ampp) {
            $amppData = array();
            
            $actual = $this->findActualNode($ampp, self::NS_SAMWS_EXPORT, 
                    'Data');
            
            if ($actual === null) {
                continue;
            }
            
            // iterate over child
            foreach ($actual->childNodes as $node) {
                
                /* @var $actualChild \DOMElement */
                switch ($node->nodeName) {
                    case 'LeafletLink':
                        $amppData['leaflets'] = $this->buildTranslatableString($node);
                        break;
                    case 'PrescriptionName':
                        $amppData['prescriptionName'] = $this
                            ->buildTranslatableString($node);
                        break;
                    case 'CrmLink':
                        $amppData['crmLink'] = $this->buildTranslatableString($node);
                        break;
                    default:
                        continue;
                }
                
            }
            
            // get CNK code 
            foreach ($ampp->childNodes as $node) {
                
                if ($node->localName === 'Dmpp') {
                    $amppData['dmpps'][]['CNK'] = $node->getAttribute('code');
                }
            }
            
            $data['ampps'][] = $amppData;
        }
        
        return $data;
    }
    
    /**
     * return all Ampp (Medical Product Package) with a 
     * Dmpp element.
     * 
     * Ampp with Dmpp are actually commercialized.
     * 
     * @param \DOMElement $el
     * @return \DOMelement[]
     */
    protected function findAmppWithDmpp(\DOMElement $el)
    {
        $ampps = $el->getElementsByTagNameNS(self::NS_SAMWS_EXPORT, 'Ampp');
        $amppsWithDmpp = array();
        
        foreach ($ampps as $ampp) {
            /* @var $amp \DOMElement */
            $dmpp = $ampp->getElementsByTagNameNS(self::NS_SAMWS_EXPORT, 'Dmpp');
            
            if ($dmpp->length === 0) {
                continue;
            }
            
            $amppsWithDmpp[] = $ampp;
        }
        
        return $amppsWithDmpp;
    }
    
    /**
     * Get all the amp components present in the element
     * 
     * @param \DOMElement $el
     * @return \DOMElement[]
     */
    protected function findAmpComponents(\DOMElement $el) 
    {
        $ampComponents = $el->getElementsByTagNameNS(self::NS_SAMWS_EXPORT, 
            'AmpComponent');
        
        return \iterator_to_array($ampComponents);
    }
    
    /**
     * Build the display name
     * 
     * @param \DOMElement $el
     * @return string[] 
     */
    protected function buildDisplayName(\DOMElement $el)
    {
        $name = $el->getElementsByTagNameNS(self::NS_SAMWS_COMMON, 
            'OfficialName')
            ->item(0)
            ->nodeValue;
        
        return \array_combine(["fr", "nl", "de", "en"], 
            \array_fill(0, 4, $name));
    }
    
    /**
     * Build a translatable string array from the child nodes.
     * 
     * Example: 
     * 
     * ```
     * <ns5:Name>
     *  <ns2:Fr>Solution injectable/pour perfusion</ns2:Fr>
     *  <ns2:Nl>Oplossing voor injectie/infusie</ns2:Nl>
     *  <ns2:De>Injektionslösung, Infusionslösung</ns2:De>
     *  <ns2:En>Solution for injection/infusion</ns2:En>
     * </ns5:Name>
     * ```
     * will be returned as 
     * 
     * ```
     * { 
     *  "fr": "Solution injectable pour perfusion",
     *  "nl": "Oplossing voor injectie/infusie",
     *  "de": "Injektionslösung, Infusionslösung",
     *  "en": "Solution for injection/infusion"
     * }
     * ```
     * 
     * @param \DOMElement $el
     * @return array an array representation of translatable string
     * @throws \RuntimeException if no child available
     */
    protected function buildTranslatableString(\DOMElement $el) 
    {
        $names = array();
        
        foreach ($el->childNodes as $node) {
            /* @var $node \DOMElement */
            $names[strtolower($node->localName)] = $node->nodeValue;
        }
        
        if (count($names) === 0) {
            throw new \RuntimeException("no name for this elements. Path is ".
                $el->getNodePath());
        }
        
        return $names;
    }
    
    /**
     * Find the actual element, according to date
     * 
     * @param \DOMElement $el the parent element
     * @param string $node_ns the NS of the element to find amongst the childNodes
     * @param string $node_tag the tag name of the element to find amongst the childNodes
     * @return \DOMElement or null if no current element
     */
    protected function findActualNode(\DOMElement $el, $node_ns, $node_tag)
    {
        /* @var $d \DOMElement */
        foreach ($el->childNodes as $d) {
            
            // check the element match the requested nodes
            if ($d->namespaceURI !== $node_ns OR $d->localName !== $node_tag) {
                continue;
            }
            
            if (!$d->hasAttribute('from')) {
                throw new \LogicException("The element $node_ns:$node_tag, "
                    . "has no from  date");
            }
            
            $from = \DateTime::createFromFormat(self::DATE_FORMAT, 
                $d->getAttribute('from'));
            
            if ($from === false) {
                throw new \LogicException("the date 'from' is invalid");
            }
            
            if ($from < $this->now) {
                // the element has a date after now. Good candidate !
                if (!$d->hasAttribute('to')) {
                    // no "to" field. Today is in the gap
                    $this->logger->debug('found element at actual date: from '.
                        $from->format('Y-m-d'). ' and no "to"');
                    return $d;
                } else {
                    $to = \DateTime::createFromFormat(self::DATE_FORMAT,
                        $d->getAttribute('to'));
                    
                    if ($to === false) {
                        throw new \Exception("The date 'to' is invalid");
                    }
                    
                    if ($to > $this->now) {
                        // ok, we are in
                        $this->logger->debug('found element at actual date : '.
                            $from->format('Y-m-d').' and to : '.
                            $to->format('Y-m-d'));
                        return $d;
                    }
                }
            }
        }
        
        $this->logger->debug('no element found');
        
        return null;
    }
    
    protected function buildXPath(\DOMElement $el)
    {
        $doc = new \DOMDocument();
        $xpath = new \DOMXPath();
    }
    
}
