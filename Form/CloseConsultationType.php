<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\Form;

use Symfony\Component\Form\AbstractType;
use Chill\HealthBundle\Form\DataTransformer\ConsultationTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Form used to close consultation
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CloseConsultationType extends AbstractType
{
    /**
     *
     * @var ConsultationTransformer
     */
    protected $transformer;
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('consultation', HiddenType::class);
        $builder->get('consultation')
            ->addModelTransformer($this->transformer);
    }

}
