<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\Form;

use Symfony\Component\Form\AbstractType;
use Chill\HealthBundle\Entity\Consultation;
use Chill\HealthBundle\Security\Authorization\ConsultationVoter;
use Chill\MainBundle\Form\Type\ChillDateType;
use Symfony\Component\Form\Test\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\MainBundle\Form\Type\ScopePickerType;
use Chill\MainBundle\Form\Type\UserPickerType;

/**
 * Form to update consultation
 * 
 * options:
 * 
 * - `center` : the center of the patient
 * - `action` : either `CREATE` or `UPDATE` constants
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ConsultationType extends AbstractType
{
    
    const CREATE = 'create';
    const UPDATE = 'update';
    
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', ChillDateType::class)
            ->add('author', UserPickerType::class, [
                'center' => $options['center'],
                'role'   => $options['step'] === self::CREATE 
                    ? ConsultationVoter::CREATE : ConsultationVoter::UPDATE
            ])
            ->add('circle', ScopePickerType::class, [
                'center' => $options['center'],
                'role'   => $options['step'] === self::CREATE
                    ? ConsultationVoter::CREATE : ConsultationVoter::UPDATE
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', Consultation::class)
            // set center option
            ->setRequired('center')
            ->setAllowedTypes('center', [\Chill\MainBundle\Entity\Center::class])
            ->setRequired('step')
            ->setAllowedValues('step', [self::UPDATE, self::CREATE])
            ;
    }

}
