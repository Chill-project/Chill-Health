<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Chill\HealthBundle\Entity\Code;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\MainBundle\Templating\TranslatableStringHelper;

/**
 * Create a form type for code
 * 
 * Possibles options :
 * 
 * - `type` (required, string) : the type of the code (e.g. substance)
 * - `code_system` (optional, string) : restrict to a certain code system
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CodeType extends AbstractType
{
    /**
     *
     * @var EntityRepository
     */
    private $er;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(
        EntityRepository $er,
        TranslatableStringHelper $translatableStringHelper
    ) {
        $this->er = $er;
        $this->translatableStringHelper = $translatableStringHelper;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            // add a "type" option
            ->setRequired('type')
            // add a "code_system" option
            ->setDefined('code_system')
            // set types
            ->setAllowedTypes([
                'code_system' => 'string',
                'type'        => 'string'
                ])
            ;
        
        // set default options for entity type
        $resolver
            ->setDefault('class', Code::class)
            ->setDefault('placeholder', 'Choice a code')
            ->setDefault('choice_label', function (Code $code) {
                        return $this->translatableStringHelper
                            ->localize($code->getDisplayName());
                    }
                )
            ->setDefault('choice_attr', function (Code $code, $key, $index) {
                return ['data-code' => \json_encode($code->getData())];
            })
            ->setDefault('attr', [
                'class' => 'select2'
            ])
            // override the query_builder's normalizer
            ->setNormalizer('query_builder', function(Options $options) {
                $qb = $this->er->createQueryBuilder('c');
                $qb->where($qb->expr()->like('c.type', ':type'))
                    ->setParameter('type', $options['type']);

                if (array_key_exists('code_system', $options)) {
                    $qb->where($qb->expr()->like('c.codeSystem', ':code_system'))
                        ->setParameter('code_system', $options['code_system']);
                }
                
                return $qb;
            })
            ;
    }
    
    public function getParent()
    {
        return EntityType::class;
    }


}
