<?php

namespace Chill\HealthBundle\Form;

use Symfony\Component\Form\AbstractType;
use Chill\HealthBundle\Form\Type\AdministrationFrequencyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Chill\HealthBundle\Form\Type\CodeType;
use Chill\HealthBundle\Form\DataTransformer\ConsultationTransformer;
use Chill\MainBundle\Form\Type\ChillDateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Chill\HealthBundle\Form\Type\UcumType;


/**
 * Form to add / remove medication
 */
class MedicationType extends AbstractType
{
    
    /**
     *
     * @var ConsultationTransformer
     */
    private $consultationTransformer;
    
    public function __construct(ConsultationTransformer $consultationTransformer)
    {
        $this->consultationTransformer = $consultationTransformer;
    }

        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateFrom', ChillDateType::class)
            ->add('substance', CodeType::class, 
                [
                    'type' => 'substance',
                    'placeholder' => 'Choose a substance'
                ])
            // removed (temporarily ?)
            /*->add('substance_string', TextType::class, array(
                'required' => false
            ))*/
            ->add('doseQuantityValue')
            ->add('doseQuantityUnit', UcumType::class)
            ->add('routeOfAdministration', CodeType::class,
                [
                    'type' => 'route_of_administration',
                    'placeholder' => 'Choose a route of administration'
                ])
            //->add('administrationUnitCode')
            ->add('administrationFrequency', AdministrationFrequencyType::class)
            ->add('dateTo', ChillDateType::class)
            ->add('modeDelivrance', TextareaType::class, array(
                'required' => false
            ))
            ->add('neverPublish', ChoiceType::class, [
                'choices' => [
                    'never publish' => true,
                    'allow publication' => false
                ],
                'choices_as_values' => true,
                'empty_data' => false
            ])
            ->add('consultation', HiddenType::class)
        ;
        
        $builder->get('consultation')
            ->addModelTransformer($this->consultationTransformer)
            ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chill\HealthBundle\Entity\Medication'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'chill_healthbundle_medication';
    }
}
