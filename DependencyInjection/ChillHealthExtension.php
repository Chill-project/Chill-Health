<?php

namespace Chill\HealthBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\HealthBundle\Security\Authorization\ConsultationVoter;
use Chill\HealthBundle\Security\Authorization\PublicationVoter;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ChillHealthExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('services/repositories.yml');
        $loader->load('services/form.yml');
        $loader->load('services/authorization.yml');
        $loader->load('services/code.yml');
        $loader->load('services/util.yml');
    }
    
    public function prepend(ContainerBuilder $container)
    {
        $this->prependRoutes($container);
        $this->prependAuthorization($container);
    }
        
    /* (non-PHPdoc)
     * @see \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface::prepend()
     */
    public function prependRoutes(ContainerBuilder $container) 
    {
        //add routes for custom bundle
         $container->prependExtensionConfig('chill_main', array(
           'routing' => array(
              'resources' => array(
                 '@ChillHealthBundle/Resources/config/routing.yml'
              )
           )
        ));
    }
    
    public function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', array(
           'role_hierarchy' => array(
              ConsultationVoter::UPDATE => array(ConsultationVoter::SEE),
              ConsultationVoter::CREATE => array(ConsultationVoter::SEE),
              PublicationVoter::CREATE  => array(PublicationVoter::SEE)
           )
        ));
    }
}
